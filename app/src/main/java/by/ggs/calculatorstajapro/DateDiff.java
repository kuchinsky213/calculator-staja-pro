package by.ggs.calculatorstajapro;

import android.app.DatePickerDialog;
import android.content.Intent;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;

import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DateDiff#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DateDiff extends Fragment implements View.OnClickListener, AdapterView.OnItemSelectedListener {



//    @BindView(R.id.textViewNextBirthdayMonths)
//    TextView textViewNextBirthdayMonths;
//    @BindView(R.id.textViewNextBirthdayDays)
//    TextView textViewNextBirthdayDays;
//    @BindView(R.id.textViewFinalYears)
//    TextView textViewFinalYears;
//    @BindView(R.id.textViewFinalMonths)
//    TextView textViewFinalMonths;
//    @BindView(R.id.textViewFinalDays)
//    TextView textViewFinalDays;
//    @BindView(R.id.textViewCurrentDay)
//    TextView textViewCurrentDay;
//    @BindView(R.id.textViewCalculate)
//    TextView textViewCalculate;
//    @BindView(R.id.textViewClear)
//    TextView textViewClear;
//    @BindView(R.id.imageViewCalenderFirst)
//    ImageView imageViewCalenderFirst;
//    @BindView(R.id.imageViewCalenderSecond)
//    ImageView imageViewCalenderSecond;
//    @BindView(R.id.editTextBirthDay)
//    EditText editTextBirthDay;
//    @BindView(R.id.editTextBirthMonth)
//    EditText editTextBirthMonth;
//    @BindView(R.id.editTextBirthYear)
//    EditText editTextBirthYear;
//    @BindView(R.id.editTextCurrentDay)
//    EditText editTextCurrentDay;
//    @BindView(R.id.editTextCurrentMonth)
//    EditText editTextCurrentMonth;
//    @BindView(R.id.editTextCurrentYear)
//    EditText editTextCurrentYear;



    int year_2, month2, day2;
    int year_3, month3, day3;
    int year_4, month4, day4;
    int year_5, month5, day5;
    LinearLayout llAge, llAge2, llAge3,  llAge4,llAge5, knopki1, knopki2, knopki3, knopki4, knopki5,LinearLayout022, LinearLayout023, LinearLayout024, LinearLayout025;

    TextView textViewNextBirthdayMonths;
      TextView textViewNextBirthdayDays;
      TextView textViewFinalYears;
      TextView textViewFinalMonths;
       TextView textViewFinalDays;
      TextView textViewCurrentDay;
      TextView textViewCalculate;
      TextView textViewClear;
       ImageView imageViewCalenderFirst;
       ImageView imageViewCalenderSecond;
       EditText editTextBirthDay;
       EditText editTextBirthMonth;

    EditText editTextBirthYear;
    EditText editTextCurrentDay;
      EditText editTextCurrentMonth;
    EditText editTextCurrentYear;

    //---------------------second

    TextView textViewNextBirthdayDays2;
    TextView textViewFinalYears2;
    TextView textViewFinalMonths2;
    TextView textViewFinalDays2;
    TextView textViewCurrentDay2;
    TextView textViewCalculate2;
    TextView textViewClear2;
    ImageView imageViewCalenderFirst2;
    ImageView imageViewCalenderSecond2;
    EditText editTextBirthDa2y;
    EditText editTextBirthMonth2;

    EditText editTextBirthYear2;
    EditText editTextCurrentDay2;
    EditText editTextCurrentMonth2;
    EditText editTextCurrentYear2;
//---------------------------------------------third
    TextView textViewNextBirthdayDays3;
    TextView textViewFinalYears3;
    TextView textViewFinalMonths3;
    TextView textViewFinalDays3;
    TextView textViewCurrentDay3;
    TextView textViewCalculate3;
    TextView textViewClear3;
    ImageView imageViewCalenderFirst3;
    ImageView imageViewCalenderSecond3;
    EditText editTextBirthDa3y;
    EditText editTextBirthMonth3;

    EditText editTextBirthYear3;
    EditText editTextCurrentDay3;
    EditText editTextCurrentMonth3;
    EditText editTextCurrentYear3;


    //---------------------------------------------fourth
    TextView textViewNextBirthdayDays4;
    TextView textViewFinalYears4;
    TextView textViewFinalMonths4;
    TextView textViewFinalDays4;
    TextView textViewCurrentDay4;
    TextView textViewCalculate4;
    TextView textViewClear4;
    ImageView imageViewCalenderFirst4;
    ImageView imageViewCalenderSecond4;
    EditText editTextBirthDa4y;
    EditText editTextBirthMonth4;

    EditText editTextBirthYear4;
    EditText editTextCurrentDay4;
    EditText editTextCurrentMonth4;
    EditText editTextCurrentYear4;


    //---------------------------------------------fifth
    TextView textViewNextBirthdayDays5;
    TextView textViewFinalYears5;
    TextView textViewFinalMonths5;
    TextView textViewFinalDays5;
    TextView textViewCurrentDay5;
    TextView textViewCalculate5;
    TextView textViewClear5;
    ImageView imageViewCalenderFirst5;
    ImageView imageViewCalenderSecond5;
    EditText editTextBirthDa5y;
    EditText editTextBirthMonth5;

    EditText editTextBirthYear5;
    EditText editTextCurrentDay5;
    EditText editTextCurrentMonth5;
    EditText editTextCurrentYear5;

    public DateDiff() {
        // Required empty public constructor
    }

    public static DateDiff newInstance(String param1, String param2) {
        DateDiff fragment = new DateDiff();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_date_diff, container, false);


        ButterKnife.bind(getActivity());
        setupCurrentDate(); // setup today's date



      //  textViewNextBirthdayMonths=view.findViewById(R.id.textViewNextBirthdayMonths);

     //   textViewNextBirthdayDays=view.findViewById(R.id.textViewNextBirthdayDays);

                textViewFinalYears=view.findViewById (R.id.textViewFinalYears);
                textViewFinalMonths=view.findViewById(R.id.textViewFinalMonths);
                 textViewFinalDays=view.findViewById(R.id.textViewFinalDays);
                textViewCurrentDay=view.findViewById(R.id.textViewCurrentDay);
                textViewCalculate=view.findViewById( R.id.textViewCalculate);
                 textViewClear=view.findViewById (R.id.textViewClear);
                 imageViewCalenderFirst=view.findViewById(R.id.imageViewCalenderFirst);
               imageViewCalenderSecond=view.findViewById(R.id.imageViewCalenderSecond);
               editTextBirthDay=view.findViewById(R.id.editTextBirthDay);
                 editTextBirthMonth=view.findViewById(R.id.editTextBirthMonth);
                editTextBirthYear=view.findViewById(R.id.editTextBirthYear);
               editTextCurrentDay=view.findViewById(R.id.editTextCurrentDay);
                editTextCurrentMonth=view.findViewById(R.id.editTextCurrentMonth);
                editTextCurrentYear=view.findViewById(R.id.editTextCurrentYear);

//------------------------------------------------------------------------------------second work


        textViewFinalYears2=view.findViewById (R.id.textViewFinalYears2);
        textViewFinalMonths2=view.findViewById(R.id.textViewFinalMonths2);
        textViewFinalDays2=view.findViewById(R.id.textViewFinalDays2);
        textViewCurrentDay2=view.findViewById(R.id.textViewCurrentDay2);
        textViewCalculate2=view.findViewById( R.id.textViewCalculate2);
        textViewClear2=view.findViewById (R.id.textViewClear2);
        imageViewCalenderFirst2=view.findViewById(R.id.imageViewCalenderFirst2);
        imageViewCalenderSecond2=view.findViewById(R.id.imageViewCalenderSecond2);
        editTextBirthDa2y=view.findViewById(R.id.editTextBirthDay2);
        editTextBirthMonth2=view.findViewById(R.id.editTextBirthMonth2);
        editTextBirthYear2=view.findViewById(R.id.editTextBirthYear2);
        editTextCurrentDay2=view.findViewById(R.id.editTextCurrentDay2);
        editTextCurrentMonth2=view.findViewById(R.id.editTextCurrentMonth2);
        editTextCurrentYear2=view.findViewById(R.id.editTextCurrentYear2);

        final Spinner spinner = (Spinner)view. findViewById(R.id.spinner);








        textViewCalculate.setOnClickListener(this);
        textViewClear.setOnClickListener(this);
        imageViewCalenderSecond.setOnClickListener(this);
        imageViewCalenderFirst.setOnClickListener(this);

        //-----------------------------------------second


        textViewCalculate2.setOnClickListener(this);
        textViewClear2.setOnClickListener(this);
        imageViewCalenderSecond2.setOnClickListener(this);
        imageViewCalenderFirst2.setOnClickListener(this);



       llAge = (LinearLayout)view.findViewById(R.id.llAge);
       llAge2 = (LinearLayout)view.findViewById(R.id.llAge2);
       knopki2 = (LinearLayout)view.findViewById(R.id.knopki2);
       knopki1 = (LinearLayout)view.findViewById(R.id.knopki1);
        LinearLayout022 = (LinearLayout)view.findViewById(R.id.LinearLayout022);


        //--------------------------------------------------------------------------------third





        textViewFinalYears3=view.findViewById (R.id.textViewFinalYears3);
        textViewFinalMonths3=view.findViewById(R.id.textViewFinalMonths3);
        textViewFinalDays3=view.findViewById(R.id.textViewFinalDays3);
        textViewCurrentDay3=view.findViewById(R.id.textViewCurrentDay3);
        textViewCalculate3=view.findViewById( R.id.textViewCalculate3);
        textViewClear3=view.findViewById (R.id.textViewClear3);
        imageViewCalenderFirst3=view.findViewById(R.id.imageViewCalenderFirst3);
        imageViewCalenderSecond3=view.findViewById(R.id.imageViewCalenderSecond3);
        editTextBirthDa3y=view.findViewById(R.id.editTextBirthDay3);
        editTextBirthMonth3=view.findViewById(R.id.editTextBirthMonth3);
        editTextBirthYear3=view.findViewById(R.id.editTextBirthYear3);
        editTextCurrentDay3=view.findViewById(R.id.editTextCurrentDay3);
        editTextCurrentMonth3=view.findViewById(R.id.editTextCurrentMonth3);
        editTextCurrentYear3=view.findViewById(R.id.editTextCurrentYear3);











        textViewCalculate3.setOnClickListener(this);
        textViewClear3.setOnClickListener(this);
        imageViewCalenderSecond3.setOnClickListener(this);
        imageViewCalenderFirst3.setOnClickListener(this);




        llAge3 = (LinearLayout)view.findViewById(R.id.llAge3);
        knopki3 = (LinearLayout)view.findViewById(R.id.knopki3);
        LinearLayout023 = (LinearLayout)view.findViewById(R.id.LinearLayout023);





















        //-------------------------------------------------------------------------------------------


        //--------------------------------------------------------------------------------fourth





        textViewFinalYears4=view.findViewById (R.id.textViewFinalYears4);
        textViewFinalMonths4=view.findViewById(R.id.textViewFinalMonths4);
        textViewFinalDays4=view.findViewById(R.id.textViewFinalDays4);
        textViewCurrentDay4=view.findViewById(R.id.textViewCurrentDay4);
        textViewCalculate4=view.findViewById( R.id.textViewCalculate4);
        textViewClear4=view.findViewById (R.id.textViewClear4);
        imageViewCalenderFirst4=view.findViewById(R.id.imageViewCalenderFirst4);
        imageViewCalenderSecond4=view.findViewById(R.id.imageViewCalenderSecond4);
        editTextBirthDa4y=view.findViewById(R.id.editTextBirthDay4);
        editTextBirthMonth4=view.findViewById(R.id.editTextBirthMonth4);
        editTextBirthYear4=view.findViewById(R.id.editTextBirthYear4);
        editTextCurrentDay4=view.findViewById(R.id.editTextCurrentDay4);
        editTextCurrentMonth4=view.findViewById(R.id.editTextCurrentMonth4);
        editTextCurrentYear4=view.findViewById(R.id.editTextCurrentYear4);











        textViewCalculate4.setOnClickListener(this);
        textViewClear4.setOnClickListener(this);
        imageViewCalenderSecond4.setOnClickListener(this);
        imageViewCalenderFirst4.setOnClickListener(this);




        llAge4 = (LinearLayout)view.findViewById(R.id.llAge4);
        knopki4 = (LinearLayout)view.findViewById(R.id.knopki4);
        LinearLayout024 = (LinearLayout)view.findViewById(R.id.LinearLayout024);





















        //-------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------fives





        textViewFinalYears5=view.findViewById (R.id.textViewFinalYears5);
        textViewFinalMonths5=view.findViewById(R.id.textViewFinalMonths5);
        textViewFinalDays5=view.findViewById(R.id.textViewFinalDays5);
        textViewCurrentDay5=view.findViewById(R.id.textViewCurrentDay5);
        textViewCalculate5=view.findViewById( R.id.textViewCalculate5);
        textViewClear5=view.findViewById (R.id.textViewClear5);
        imageViewCalenderFirst5=view.findViewById(R.id.imageViewCalenderFirst5);
        imageViewCalenderSecond5=view.findViewById(R.id.imageViewCalenderSecond5);
        editTextBirthDa5y=view.findViewById(R.id.editTextBirthDay5);
        editTextBirthMonth5=view.findViewById(R.id.editTextBirthMonth5);
        editTextBirthYear5=view.findViewById(R.id.editTextBirthYear5);
        editTextCurrentDay5=view.findViewById(R.id.editTextCurrentDay5);
        editTextCurrentMonth5=view.findViewById(R.id.editTextCurrentMonth5);
        editTextCurrentYear5=view.findViewById(R.id.editTextCurrentYear5);











        textViewCalculate5.setOnClickListener(this);
        textViewClear5.setOnClickListener(this);
        imageViewCalenderSecond5.setOnClickListener(this);
        imageViewCalenderFirst5.setOnClickListener(this);




        llAge5 = (LinearLayout)view.findViewById(R.id.llAge5);
        knopki5 = (LinearLayout)view.findViewById(R.id.knopki5);
        LinearLayout025 = (LinearLayout)view.findViewById(R.id.LinearLayout025);





















        //-------------------------------------------------------------------------------------------


        final Calendar c = Calendar.getInstance();
        editTextCurrentYear.setText(String.valueOf(c.get(Calendar.YEAR)));
        editTextCurrentMonth.setText(addZero(c.get(Calendar.MONTH) + 1));
        editTextCurrentDay.setText(addZero(c.get(Calendar.DAY_OF_MONTH)));

        SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
        Date date = new Date(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH) - 1);
        String dayOfWeek = simpledateformat.format(date);
        textViewCurrentDay.setText(dayOfWeek);
        textViewCurrentDay.setVisibility(View.VISIBLE);



        spinner.setOnItemSelectedListener(DateDiff.this);

        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("1");
        categories.add("2");
        categories.add("3");
        categories.add("4");
        categories.add("5");


        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(),
                R.layout.color_spinner_layout,
                categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.spinner_dropdown_layout);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);










        // Inflate the layout for this fragment
        return view;
    }

    private void setupCurrentDate() {

    }

    private String addZero(int number) {
        String n;
        if (number < 10) {
            n = "0" + number;
        } else {
            n = String.valueOf(number);
        }
        return n;
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.imageViewCalenderSecond) {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {

                    editTextBirthDay.setText(addZero(dayOfMonth));
                    editTextBirthMonth.setText(addZero(monthOfYear + 1));
                    editTextBirthYear.setText(String.valueOf(year));
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.show();
        } else if (view == textViewCalculate) {
            if (!TextUtils.isEmpty(editTextBirthDay.getText()) && !TextUtils.isEmpty(editTextBirthMonth.getText()) && !TextUtils.isEmpty(editTextBirthYear.getText())) {
                calculateAge();


                llAge.setVisibility(view.VISIBLE);
           //     nextBirthday();
            } else {

                Toasty.warning(getContext(), (R.string.zapolni), Toast.LENGTH_SHORT, true).show();
            }
        } else if (view == textViewClear) {
            editTextBirthDay.setText("");
            editTextBirthMonth.setText("");
            editTextBirthYear.setText("");

            editTextCurrentDay.setText("");
            editTextCurrentMonth.setText("");
            editTextCurrentYear.setText("");
            Toasty.success(getContext(), (R.string.uspeshno), Toast.LENGTH_SHORT, true).show();
        }




        //----------------------second

        else if (view == textViewCalculate2) {
            if (!TextUtils.isEmpty(editTextBirthDa2y.getText()) && !TextUtils.isEmpty(editTextBirthMonth2.getText()) && !TextUtils.isEmpty(editTextBirthYear2.getText())) {
                calculateAge2();

                if (!TextUtils.isEmpty(editTextBirthDay.getText()) && !TextUtils.isEmpty(editTextBirthMonth.getText()) && !TextUtils.isEmpty(editTextBirthYear.getText())) {
                    calculateAge();


                  //  llAge.setVisibility(view.VISIBLE);
                    //     nextBirthday();
                } else {

                    Toasty.warning(getContext(), (R.string.zapolni), Toast.LENGTH_SHORT, true).show();
                }
              //  llAge.setVisibility(view.VISIBLE);
                //     nextBirthday();
            } else {

                Toasty.warning(getContext(), (R.string.zapolni), Toast.LENGTH_SHORT, true).show();
            }}


        else if (view == textViewClear2) {

            editTextBirthDay.setText("");
            editTextBirthMonth.setText("");
            editTextBirthYear.setText("");

            editTextCurrentDay.setText("");
            editTextCurrentMonth.setText("");
            editTextCurrentYear.setText("");
            editTextBirthDa2y.setText("");
            editTextBirthMonth2.setText("");
            editTextBirthYear2.setText("");

            editTextCurrentDay2.setText("");
            editTextCurrentMonth2.setText("");
            editTextCurrentYear2.setText("");
            Toasty.success(getContext(), (R.string.uspeshno), Toast.LENGTH_SHORT, true).show();
        }


        else if (view.getId() == R.id.imageViewCalenderFirst) {
            final Calendar c = Calendar.getInstance();
            int mYear1 = c.get(Calendar.YEAR);
            int mMonth1 = c.get(Calendar.MONTH);
            int mDay1 = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {

                    editTextCurrentDay.setText(addZero(dayOfMonth));
                    editTextCurrentMonth.setText(addZero(monthOfYear + 1));
                    editTextCurrentYear.setText(String.valueOf(year));
                }
            }, mYear1, mMonth1, mDay1);
            datePickerDialog.show();
        }

        else if (view.getId() == R.id.imageViewCalenderSecond2) {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {

                    editTextBirthDa2y.setText(addZero(dayOfMonth));
                    editTextBirthMonth2.setText(addZero(monthOfYear + 1));
                    editTextBirthYear2.setText(String.valueOf(year));
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }

        else if (view.getId() == R.id.imageViewCalenderFirst2) {
            final Calendar c = Calendar.getInstance();
            int mYear1 = c.get(Calendar.YEAR);
            int mMonth1 = c.get(Calendar.MONTH);
            int mDay1 = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {

                    editTextCurrentDay2.setText(addZero(dayOfMonth));
                    editTextCurrentMonth2.setText(addZero(monthOfYear + 1));
                    editTextCurrentYear2.setText(String.valueOf(year));
                }
            }, mYear1, mMonth1, mDay1);
            datePickerDialog.show();
        }
//----------------------third

        else if (view == textViewCalculate3) {
            if (!TextUtils.isEmpty(editTextBirthDay.getText()) && !TextUtils.isEmpty(editTextBirthMonth.getText()) && !TextUtils.isEmpty(editTextBirthYear.getText())) {
                calculateAge();}   else {

                Toasty.warning(getContext(), (R.string.zapolni), Toast.LENGTH_SHORT, true).show();
            }

                  if (!TextUtils.isEmpty(editTextBirthDa2y.getText()) && !TextUtils.isEmpty(editTextBirthMonth2.getText()) && !TextUtils.isEmpty(editTextBirthYear2.getText())
                        && !TextUtils.isEmpty(editTextCurrentDay2.getText()) && !TextUtils.isEmpty(editTextCurrentMonth2.getText()) && !TextUtils.isEmpty(editTextCurrentYear2.getText())

                  ) {




                    calculateAge2();}
                     else {

                      Toasty.warning(getContext(), (R.string.zapolni), Toast.LENGTH_SHORT, true).show();
                      }
                        if (!TextUtils.isEmpty(editTextBirthDa3y.getText()) && !TextUtils.isEmpty(editTextBirthMonth3.getText()) && !TextUtils.isEmpty(editTextBirthYear3.getText())) {
                        calculateAge3();
                  //  llAge.setVisibility(view.VISIBLE);
                //     nextBirthday();
                //  llAge.setVisibility(view.VISIBLE);
                //     nextBirthday();
            } else {

                            Toasty.warning(getContext(), (R.string.zapolni), Toast.LENGTH_SHORT, true).show();
            }}


        else if (view == textViewClear3) {

            editTextBirthDay.setText("");
            editTextBirthMonth.setText("");
            editTextBirthYear.setText("");

            editTextCurrentDay.setText("");
            editTextCurrentMonth.setText("");
            editTextCurrentYear.setText("");
            editTextBirthDa2y.setText("");
            editTextBirthMonth2.setText("");
            editTextBirthYear2.setText("");

            editTextCurrentDay2.setText("");
            editTextCurrentMonth2.setText("");
            editTextCurrentYear2.setText("");




            editTextBirthDa3y.setText("");
            editTextBirthMonth3.setText("");
            editTextBirthYear3.setText("");

            editTextCurrentDay3.setText("");
            editTextCurrentMonth3.setText("");
            editTextCurrentYear3.setText("");
            Toasty.success(getContext(), (R.string.uspeshno), Toast.LENGTH_SHORT, true).show();
        }


        else if (view.getId() == R.id.imageViewCalenderSecond3) {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {

                    editTextBirthDa3y.setText(addZero(dayOfMonth));
                    editTextBirthMonth3.setText(addZero(monthOfYear + 1));
                    editTextBirthYear3.setText(String.valueOf(year));
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }

        else if (view.getId() == R.id.imageViewCalenderFirst3) {
            final Calendar c = Calendar.getInstance();
            int mYear1 = c.get(Calendar.YEAR);
            int mMonth1 = c.get(Calendar.MONTH);
            int mDay1 = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {

                    editTextCurrentDay3.setText(addZero(dayOfMonth));
                    editTextCurrentMonth3.setText(addZero(monthOfYear + 1));
                    editTextCurrentYear3.setText(String.valueOf(year));
                }
            }, mYear1, mMonth1, mDay1);
            datePickerDialog.show();
        }







        //----------------------fourth

        else if (view == textViewCalculate4
        ) {

            if (!TextUtils.isEmpty(editTextBirthDay.getText()) && !TextUtils.isEmpty(editTextBirthMonth.getText()) && !TextUtils.isEmpty(editTextBirthYear.getText())
                    && !TextUtils.isEmpty(editTextCurrentDay.getText()) && !TextUtils.isEmpty(editTextCurrentMonth.getText()) && !TextUtils.isEmpty(editTextCurrentYear.getText()))
            {
                calculateAge();}   else {
                Toasty.warning(getContext(), (R.string.zapolni), Toast.LENGTH_SHORT, true).show();
            }

            if (!TextUtils.isEmpty(editTextBirthDa2y.getText()) && !TextUtils.isEmpty(editTextBirthMonth2.getText()) && !TextUtils.isEmpty(editTextBirthYear2.getText())
                    && !TextUtils.isEmpty(editTextCurrentDay2.getText()) && !TextUtils.isEmpty(editTextCurrentMonth2.getText()) && !TextUtils.isEmpty(editTextCurrentYear2.getText())

            ) {
                calculateAge2();}
            else {

                Toasty.warning(getContext(), (R.string.zapolni), Toast.LENGTH_SHORT, true).show();
            }
            if (!TextUtils.isEmpty(editTextBirthDa3y.getText()) && !TextUtils.isEmpty(editTextBirthMonth3.getText()) && !TextUtils.isEmpty(editTextBirthYear3.getText())
                    && !TextUtils.isEmpty(editTextCurrentDay3.getText()) && !TextUtils.isEmpty(editTextCurrentMonth3.getText()) && !TextUtils.isEmpty(editTextCurrentYear3.getText()))

            {
                calculateAge3();
            } else {
                Toasty.warning(getContext(), (R.string.zapolni), Toast.LENGTH_SHORT, true).show();
            }


            if (!TextUtils.isEmpty(editTextBirthDa4y.getText()) && !TextUtils.isEmpty(editTextBirthMonth4.getText()) &&
                    !TextUtils.isEmpty(editTextBirthYear4.getText())
                    && !TextUtils.isEmpty(editTextCurrentDay4.getText()) && !TextUtils.isEmpty(editTextCurrentMonth4.getText()) &&
                    !TextUtils.isEmpty(editTextCurrentYear4.getText())

            ) {
                calculateAge4();

            } else{
                Toasty.warning(getContext(), (R.string.zapolni), Toast.LENGTH_SHORT, true).show();
            }



        }


        else if (view == textViewClear4) {

            editTextBirthDay.setText("");
            editTextBirthMonth.setText("");
            editTextBirthYear.setText("");

            editTextCurrentDay.setText("");
            editTextCurrentMonth.setText("");
            editTextCurrentYear.setText("");
            editTextBirthDa2y.setText("");
            editTextBirthMonth2.setText("");
            editTextBirthYear2.setText("");

            editTextCurrentDay2.setText("");
            editTextCurrentMonth2.setText("");
            editTextCurrentYear2.setText("");




            editTextBirthDa3y.setText("");
            editTextBirthMonth3.setText("");
            editTextBirthYear3.setText("");

            editTextCurrentDay3.setText("");
            editTextCurrentMonth3.setText("");
            editTextCurrentYear3.setText("");


            editTextBirthDa4y.setText("");
            editTextBirthMonth4.setText("");
            editTextBirthYear4.setText("");

            editTextCurrentDay4.setText("");
            editTextCurrentMonth4.setText("");
            editTextCurrentYear4.setText("");
            Toasty.success(getContext(), (R.string.uspeshno), Toast.LENGTH_SHORT, true).show();
        }


        else if (view.getId() == R.id.imageViewCalenderSecond4) {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {

                    editTextBirthDa4y.setText(addZero(dayOfMonth));
                    editTextBirthMonth4.setText(addZero(monthOfYear + 1));
                    editTextBirthYear4.setText(String.valueOf(year));
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }

        else if (view.getId() == R.id.imageViewCalenderFirst4) {
            final Calendar c = Calendar.getInstance();
            int mYear1 = c.get(Calendar.YEAR);
            int mMonth1 = c.get(Calendar.MONTH);
            int mDay1 = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {

                    editTextCurrentDay4.setText(addZero(dayOfMonth));
                    editTextCurrentMonth4.setText(addZero(monthOfYear + 1));
                    editTextCurrentYear4.setText(String.valueOf(year));
                }
            }, mYear1, mMonth1, mDay1);
            datePickerDialog.show();
        }






        //----------------------fives

        else if (view == textViewCalculate5
        ) {

            if (!TextUtils.isEmpty(editTextBirthDa5y.getText()) && !TextUtils.isEmpty(editTextBirthMonth5.getText()) &&
                    !TextUtils.isEmpty(editTextBirthYear5.getText())
                    && !TextUtils.isEmpty(editTextCurrentDay5.getText()) && !TextUtils.isEmpty(editTextCurrentMonth5.getText()) &&
                    !TextUtils.isEmpty(editTextCurrentYear5.getText())
                    &&
                    !TextUtils.isEmpty(editTextBirthDa4y.getText()) && !TextUtils.isEmpty(editTextBirthMonth4.getText()) &&
                    !TextUtils.isEmpty(editTextBirthYear4.getText())
                    && !TextUtils.isEmpty(editTextCurrentDay4.getText()) && !TextUtils.isEmpty(editTextCurrentMonth4.getText()) &&
                    !TextUtils.isEmpty(editTextCurrentYear4.getText())
                    &&
                    !TextUtils.isEmpty(editTextBirthDa3y.getText()) && !TextUtils.isEmpty(editTextBirthMonth3.getText()) &&
                    !TextUtils.isEmpty(editTextBirthYear3.getText())
                    && !TextUtils.isEmpty(editTextCurrentDay3.getText()) && !TextUtils.isEmpty(editTextCurrentMonth3.getText()) &&
                    !TextUtils.isEmpty(editTextCurrentYear3.getText())
                    &&
                    !TextUtils.isEmpty(editTextBirthDa2y.getText()) && !TextUtils.isEmpty(editTextBirthMonth2.getText()) &&
                    !TextUtils.isEmpty(editTextBirthYear2.getText())
                    && !TextUtils.isEmpty(editTextCurrentDay2.getText()) && !TextUtils.isEmpty(editTextCurrentMonth2.getText()) &&
                    !TextUtils.isEmpty(editTextCurrentYear2.getText())
                    &&
                    !TextUtils.isEmpty(editTextBirthDay.getText()) && !TextUtils.isEmpty(editTextBirthMonth.getText()) &&
                    !TextUtils.isEmpty(editTextBirthYear.getText())
                    && !TextUtils.isEmpty(editTextCurrentDay.getText()) && !TextUtils.isEmpty(editTextCurrentMonth.getText()) &&
                    !TextUtils.isEmpty(editTextCurrentYear.getText())

            ) {
                calculateAge();
                calculateAge2();
                calculateAge3();
                calculateAge4();
                calculateAge5();

            } else{
                Toasty.warning(getContext(), (R.string.zapolni), Toast.LENGTH_SHORT, true).show();
            }


        }


        else if (view == textViewClear5) {

            editTextBirthDay.setText("");
            editTextBirthMonth.setText("");
            editTextBirthYear.setText("");

            editTextCurrentDay.setText("");
            editTextCurrentMonth.setText("");
            editTextCurrentYear.setText("");
            editTextBirthDa2y.setText("");
            editTextBirthMonth2.setText("");
            editTextBirthYear2.setText("");

            editTextCurrentDay2.setText("");
            editTextCurrentMonth2.setText("");
            editTextCurrentYear2.setText("");

            editTextBirthDa3y.setText("");
            editTextBirthMonth3.setText("");
            editTextBirthYear3.setText("");

            editTextCurrentDay3.setText("");
            editTextCurrentMonth3.setText("");
            editTextCurrentYear3.setText("");


            editTextBirthDa4y.setText("");
            editTextBirthMonth4.setText("");
            editTextBirthYear4.setText("");
            editTextCurrentDay4.setText("");
            editTextCurrentMonth4.setText("");
            editTextCurrentYear4.setText("");

            editTextBirthDa5y.setText("");
            editTextBirthMonth5.setText("");
            editTextBirthYear5.setText("");
            editTextCurrentDay5.setText("");
            editTextCurrentMonth5.setText("");
            editTextCurrentYear5.setText("");
            Toasty.success(getContext(), (R.string.uspeshno), Toast.LENGTH_SHORT, true).show();
        }


        else if (view.getId() == R.id.imageViewCalenderSecond5) {
            final Calendar c = Calendar.getInstance();
            int mYear = c.get(Calendar.YEAR);
            int mMonth = c.get(Calendar.MONTH);
            int mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {

                    editTextBirthDa5y.setText(addZero(dayOfMonth));
                    editTextBirthMonth5.setText(addZero(monthOfYear + 1));
                    editTextBirthYear5.setText(String.valueOf(year));
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.show();
        }

        else if (view.getId() == R.id.imageViewCalenderFirst5) {
            final Calendar c = Calendar.getInstance();
            int mYear1 = c.get(Calendar.YEAR);
            int mMonth1 = c.get(Calendar.MONTH);
            int mDay1 = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {

                    editTextCurrentDay5.setText(addZero(dayOfMonth));
                    editTextCurrentMonth5.setText(addZero(monthOfYear + 1));
                    editTextCurrentYear5.setText(String.valueOf(year));
                }
            }, mYear1, mMonth1, mDay1);
            datePickerDialog.show();
        }








    }



    private void nextBirthday() {
        int currentDay = Integer.valueOf(editTextCurrentDay.getText().toString());
        int currentMonth = Integer.valueOf(editTextCurrentMonth.getText().toString());
        int currentYear = Integer.valueOf(editTextCurrentYear.getText().toString());

        Calendar current = Calendar.getInstance();
        current.set(currentYear, currentMonth, currentDay);

        int birthDay = Integer.valueOf(editTextBirthDay.getText().toString());
        int birthMonth = Integer.valueOf(editTextBirthMonth.getText().toString());
        int birthYear = Integer.valueOf(editTextBirthYear.getText().toString());

        Calendar birthday = Calendar.getInstance();
        birthday.set(birthYear, birthMonth, birthDay);

        long difference = birthday.getTimeInMillis() - current.getTimeInMillis();

        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(difference);

//        textViewNextBirthdayMonths.setText(String.valueOf(cal.get(Calendar.MONTH)));
        textViewNextBirthdayDays.setText(String.valueOf(cal.get(Calendar.DAY_OF_MONTH)));
    }

    private void calculateAge() {
        int currentDay = Integer.valueOf(editTextCurrentDay.getText().toString());
        int currentMonth = Integer.valueOf(editTextCurrentMonth.getText().toString());
        int currentYear = Integer.valueOf(editTextCurrentYear.getText().toString());

        Date now = new Date(currentYear, currentMonth, currentDay);

        int birthDay = Integer.valueOf(editTextBirthDay.getText().toString());
        int birthMonth = Integer.valueOf(editTextBirthMonth.getText().toString());
        int birthYear = Integer.valueOf(editTextBirthYear.getText().toString());

        Date dob = new Date(birthYear, birthMonth, birthDay);

        if (dob.after(now)) {
            Toasty.error(getContext(), (R.string.lol), Toast.LENGTH_SHORT, true).show();
            return;
        }
        // days of every month
        int month[] = {31, 28, 31, 30, 31, 30, 31,
                31, 30, 31, 30, 31};

        // if birth date is greater then current birth
        // month then do not count this month and add 30
        // to the date so as to subtract the date and
        // get the remaining days
        if (birthDay > currentDay) {
            currentDay = currentDay + month[birthMonth - 1];
            currentMonth = currentMonth - 1;
        }

        // if birth month exceeds current month, then do
        // not count this year and add 12 to the month so
        // that we can subtract and find out the difference
        if (birthMonth > currentMonth) {
            currentYear = currentYear - 1;
            currentMonth = currentMonth + 12;
        }

        // calculate date, month, year
        int calculated_date = currentDay - birthDay;
        int calculated_month = currentMonth - birthMonth;
        int calculated_year = currentYear - birthYear;

        year_2=calculated_year;
        month2=calculated_month;
        day2=calculated_date;


        textViewFinalDays.setText(String.valueOf(calculated_date));
        textViewFinalMonths.setText(String.valueOf(calculated_month));
        textViewFinalYears.setText(String.valueOf(calculated_year));
    }

    private void calculateAge2() {
        calculateAge();
        int currentDay = Integer.valueOf(editTextCurrentDay2.getText().toString());
        int currentMonth = Integer.valueOf(editTextCurrentMonth2.getText().toString());
        int currentYear = Integer.valueOf(editTextCurrentYear2.getText().toString());

        Date now = new Date(currentYear, currentMonth, currentDay);

        int birthDay = Integer.valueOf(editTextBirthDa2y.getText().toString());
        int birthMonth = Integer.valueOf(editTextBirthMonth2.getText().toString());
        int birthYear = Integer.valueOf(editTextBirthYear2.getText().toString());

        Date dob = new Date(birthYear, birthMonth, birthDay);


        int birthDay2 = Integer.valueOf(editTextCurrentDay.getText().toString());
        int birthMonth2 = Integer.valueOf(editTextCurrentMonth.getText().toString());
        int birthYear2 = Integer.valueOf(editTextCurrentYear.getText().toString());

        Date dob2 = new Date(birthYear2, birthMonth2, birthDay2);

        if (dob.after(now)) {
            Toasty.error(getContext(), (R.string.lol), Toast.LENGTH_SHORT, true).show();
            return;
        }
        if (dob2.after(dob)) {
            Toasty.error(getContext(), (R.string.check), Toast.LENGTH_SHORT, true).show();
            return;
        }
        // days of every month
        int month[] = {31, 28, 31, 30, 31, 30, 31,
                31, 30, 31, 30, 31};

        // if birth date is greater then current birth
        // month then do not count this month and add 30
        // to the date so as to subtract the date and
        // get the remaining days
        if (birthDay > currentDay) {
            currentDay = currentDay + month[birthMonth - 1];
            currentMonth = currentMonth - 1;
        }

        // if birth month exceeds current month, then do
        // not count this year and add 12 to the month so
        // that we can subtract and find out the difference
        if (birthMonth > currentMonth) {
            currentYear = currentYear - 1;
            currentMonth = currentMonth + 12;
        }

        // calculate date, month, year
        int calculated_date = currentDay - birthDay;
        int calculated_month = currentMonth - birthMonth;
        int calculated_year = currentYear - birthYear;


        int itog_day, itog_month, itog_year;







        itog_day= calculated_date  +day2;
        itog_month= calculated_month  +month2;
        itog_year= calculated_year  +year_2;
        if(itog_day>31){
            itog_month++;
            itog_day=itog_day-31;

        }
           if(itog_month>12){
               itog_year++;
               itog_month=itog_month-12;
           }

        year_3=itog_year;
        month3=itog_month;
        day3=itog_day;
        textViewFinalDays2.setText(String.valueOf(itog_day));
        textViewFinalMonths2.setText(String.valueOf(itog_month));
        textViewFinalYears2.setText(String.valueOf(itog_year));
    }



    private void calculateAge3() {
        //calculateAge();
       // calculateAge2();
        int currentDay = Integer.valueOf(editTextCurrentDay3.getText().toString());
        int currentMonth = Integer.valueOf(editTextCurrentMonth3.getText().toString());
        int currentYear = Integer.valueOf(editTextCurrentYear3.getText().toString());

        Date now = new Date(currentYear, currentMonth, currentDay);

        int birthDay = Integer.valueOf(editTextBirthDa3y.getText().toString());
        int birthMonth = Integer.valueOf(editTextBirthMonth3.getText().toString());
        int birthYear = Integer.valueOf(editTextBirthYear3.getText().toString());

        Date dob = new Date(birthYear, birthMonth, birthDay);


        int birthDay2 = Integer.valueOf(editTextCurrentDay.getText().toString());
        int birthMonth2 = Integer.valueOf(editTextCurrentMonth.getText().toString());
        int birthYear2 = Integer.valueOf(editTextCurrentYear.getText().toString());

        Date dob2 = new Date(birthYear2, birthMonth2, birthDay2);

        int birthDay3 = Integer.valueOf(editTextCurrentDay2.getText().toString());
        int birthMonth3 = Integer.valueOf(editTextCurrentMonth2.getText().toString());
        int birthYear3 = Integer.valueOf(editTextCurrentYear2.getText().toString());

        Date dob3 = new Date(birthYear3, birthMonth3, birthDay3);

        if (dob.after(now)) {
            Toasty.error(getContext(), (R.string.lol), Toast.LENGTH_SHORT, true).show();
            return;
        }
        if (dob2.after(dob)) {
            Toasty.error(getContext(), (R.string.check), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (dob3.after(dob)) {
            Toasty.error(getContext(), (R.string.check), Toast.LENGTH_SHORT, true).show();
            return;
        }
        // days of every month
        int month[] = {31, 28, 31, 30, 31, 30, 31,
                31, 30, 31, 30, 31};

        // if birth date is greater then current birth
        // month then do not count this month and add 30
        // to the date so as to subtract the date and
        // get the remaining days
        if (birthDay > currentDay) {
            currentDay = currentDay + month[birthMonth - 1];
            currentMonth = currentMonth - 1;
        }

        // if birth month exceeds current month, then do
        // not count this year and add 12 to the month so
        // that we can subtract and find out the difference
        if (birthMonth > currentMonth) {
            currentYear = currentYear - 1;
            currentMonth = currentMonth + 12;
        }

        // calculate date, month, year
        int calculated_date = currentDay - birthDay;
        int calculated_month = currentMonth - birthMonth;
        int calculated_year = currentYear - birthYear;


        int itog_day, itog_month, itog_year;



        itog_day= calculated_date  +day3;
        itog_month= calculated_month  +month3;
        itog_year= calculated_year  +year_3;
        if(itog_day>31){
            itog_month++;
            itog_day=itog_day-31;

        }
        if(itog_month>12){
            itog_year++;
            itog_month=itog_month-12;
        }

        year_4=itog_year;
        month4=itog_month;
        day4=itog_day;


        textViewFinalDays3.setText(String.valueOf(itog_day));
        textViewFinalMonths3.setText(String.valueOf(itog_month));
        textViewFinalYears3.setText(String.valueOf(itog_year));
    }





    private void calculateAge4() {
        //calculateAge();
        // calculateAge2();
        int currentDay = Integer.valueOf(editTextCurrentDay4.getText().toString());
        int currentMonth = Integer.valueOf(editTextCurrentMonth4.getText().toString());
        int currentYear = Integer.valueOf(editTextCurrentYear4.getText().toString());

        Date now = new Date(currentYear, currentMonth, currentDay);

        int birthDay = Integer.valueOf(editTextBirthDa4y.getText().toString());
        int birthMonth = Integer.valueOf(editTextBirthMonth4.getText().toString());
        int birthYear = Integer.valueOf(editTextBirthYear4.getText().toString());

        Date dob = new Date(birthYear, birthMonth, birthDay);

        int birthDay2 = Integer.valueOf(editTextCurrentDay.getText().toString());
        int birthMonth2 = Integer.valueOf(editTextCurrentMonth.getText().toString());
        int birthYear2 = Integer.valueOf(editTextCurrentYear.getText().toString());

        Date dob2 = new Date(birthYear2, birthMonth2, birthDay2);

        int birthDay3 = Integer.valueOf(editTextCurrentDay2.getText().toString());
        int birthMonth3 = Integer.valueOf(editTextCurrentMonth2.getText().toString());
        int birthYear3 = Integer.valueOf(editTextCurrentYear2.getText().toString());

        Date dob3 = new Date(birthYear3, birthMonth3, birthDay3);

                int birthDay4 = Integer.valueOf(editTextCurrentDay3.getText().toString());
        int birthMonth4 = Integer.valueOf(editTextCurrentMonth3.getText().toString());
        int birthYear4 = Integer.valueOf(editTextCurrentYear3.getText().toString());

        Date dob4 = new Date(birthYear4, birthMonth4, birthDay4);


        if (dob.after(now)) {
            Toasty.error(getContext(), (R.string.lol), Toast.LENGTH_SHORT, true).show();
            return;
        }
        if (dob2.after(dob)) {
            Toasty.error(getContext(), (R.string.check), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (dob3.after(dob)) {
            Toasty.error(getContext(), (R.string.check), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (dob4.after(dob)) {
            Toasty.error(getContext(), (R.string.check), Toast.LENGTH_SHORT, true).show();
            return;
        }
        // days of every month
        int month[] = {31, 28, 31, 30, 31, 30, 31,
                31, 30, 31, 30, 31};

        if (birthDay > currentDay) {
            currentDay = currentDay + month[birthMonth - 1];
            currentMonth = currentMonth - 1;
        }


        if (birthMonth > currentMonth) {
            currentYear = currentYear - 1;
            currentMonth = currentMonth + 12;
        }

        // calculate date, month, year
        int calculated_date = currentDay - birthDay;
        int calculated_month = currentMonth - birthMonth;
        int calculated_year = currentYear - birthYear;


        int itog_day, itog_month, itog_year;



        itog_day= calculated_date  +day4;
        itog_month= calculated_month  +month4;
        itog_year= calculated_year  +year_4;
        if(itog_day>31){
            itog_month++;
            itog_day=itog_day-31;

        }
        if(itog_month>12){
            itog_year++;
            itog_month=itog_month-12;
        }

        year_5=itog_year;
        month5=itog_month;
        day5=itog_day;


        textViewFinalDays4.setText(String.valueOf(itog_day));
        textViewFinalMonths4.setText(String.valueOf(itog_month));
        textViewFinalYears4.setText(String.valueOf(itog_year));
    }

    private void calculateAge5() {

        int currentDay = Integer.valueOf(editTextCurrentDay5.getText().toString());
        int currentMonth = Integer.valueOf(editTextCurrentMonth5.getText().toString());
        int currentYear = Integer.valueOf(editTextCurrentYear5.getText().toString());

        Date now = new Date(currentYear, currentMonth, currentDay);

        int birthDay = Integer.valueOf(editTextBirthDa5y.getText().toString());
        int birthMonth = Integer.valueOf(editTextBirthMonth5.getText().toString());
        int birthYear = Integer.valueOf(editTextBirthYear5.getText().toString());

        Date dob = new Date(birthYear, birthMonth, birthDay);


        int birthDay2 = Integer.valueOf(editTextCurrentDay.getText().toString());
        int birthMonth2 = Integer.valueOf(editTextCurrentMonth.getText().toString());
        int birthYear2 = Integer.valueOf(editTextCurrentYear.getText().toString());

        Date dob2 = new Date(birthYear2, birthMonth2, birthDay2);

        int birthDay3 = Integer.valueOf(editTextCurrentDay2.getText().toString());
        int birthMonth3 = Integer.valueOf(editTextCurrentMonth2.getText().toString());
        int birthYear3 = Integer.valueOf(editTextCurrentYear2.getText().toString());

        Date dob3 = new Date(birthYear3, birthMonth3, birthDay3);

        int birthDay4 = Integer.valueOf(editTextCurrentDay3.getText().toString());
        int birthMonth4 = Integer.valueOf(editTextCurrentMonth3.getText().toString());
        int birthYear4 = Integer.valueOf(editTextCurrentYear3.getText().toString());

        Date dob4 = new Date(birthYear4, birthMonth4, birthDay4);

        int birthDay5 = Integer.valueOf(editTextCurrentDay4.getText().toString());
        int birthMonth5 = Integer.valueOf(editTextCurrentMonth4.getText().toString());
        int birthYear5 = Integer.valueOf(editTextCurrentYear4.getText().toString());

        Date dob5 = new Date(birthYear5, birthMonth5, birthDay5);


        if (dob.after(now)) {
            Toasty.error(getContext(), (R.string.lol), Toast.LENGTH_SHORT, true).show();
            return;
        }
        if (dob2.after(dob)) {
            Toasty.error(getContext(), (R.string.check), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (dob3.after(dob)) {
            Toasty.error(getContext(), (R.string.check), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (dob4.after(dob)) {
            Toasty.error(getContext(), (R.string.check), Toast.LENGTH_SHORT, true).show();
            return;
        }

        if (dob5.after(dob)) {
            Toasty.error(getContext(), (R.string.check), Toast.LENGTH_SHORT, true).show();
            return;
        }
        // days of every month
        int month[] = {31, 28, 31, 30, 31, 30, 31,
                31, 30, 31, 30, 31};

        // if birth date is greater then current birth
        // month then do not count this month and add 30
        // to the date so as to subtract the date and
        // get the remaining days
        if (birthDay > currentDay) {
            currentDay = currentDay + month[birthMonth - 1];
            currentMonth = currentMonth - 1;
        }

        // if birth month exceeds current month, then do
        // not count this year and add 12 to the month so
        // that we can subtract and find out the difference
        if (birthMonth > currentMonth) {
            currentYear = currentYear - 1;
            currentMonth = currentMonth + 12;
        }

        // calculate date, month, year
        int calculated_date = currentDay - birthDay;
        int calculated_month = currentMonth - birthMonth;
        int calculated_year = currentYear - birthYear;


        int itog_day, itog_month, itog_year;



        itog_day= calculated_date  +day5;
        itog_month= calculated_month  +month5;
        itog_year= calculated_year  +year_5;
        if(itog_day>31){
            itog_month++;
            itog_day=itog_day-31;

        }
        if(itog_month>12){
            itog_year++;
            itog_month=itog_month-12;
        }



        textViewFinalDays5.setText(String.valueOf(itog_day));
        textViewFinalMonths5.setText(String.valueOf(itog_month));
        textViewFinalYears5.setText(String.valueOf(itog_year));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();

        // Showing selected spinner item

        if(position==0){
            knopki1.setVisibility(view.VISIBLE);
            knopki2.setVisibility(view.GONE);
            knopki3.setVisibility(view.GONE);
            knopki4.setVisibility(view.GONE);
            knopki5.setVisibility(view.GONE);
            LinearLayout022.setVisibility(view.GONE);
            LinearLayout023.setVisibility(view.GONE);
            LinearLayout024.setVisibility(view.GONE);
            LinearLayout025.setVisibility(view.GONE);
            llAge.setVisibility(view.VISIBLE);
            llAge2.setVisibility(view.GONE);
            llAge3.setVisibility(view.GONE);
            llAge4.setVisibility(view.GONE);
            llAge5.setVisibility(view.GONE);

        }
        if(position==1){
            knopki1.setVisibility(view.GONE);
            knopki2.setVisibility(view.VISIBLE);
            knopki3.setVisibility(view.GONE);
            knopki4.setVisibility(view.GONE);
            knopki5.setVisibility(view.GONE);
            LinearLayout022.setVisibility(view.VISIBLE);
            LinearLayout023.setVisibility(view.GONE);
            LinearLayout024.setVisibility(view.GONE);
            LinearLayout025.setVisibility(view.GONE);
            llAge.setVisibility(view.GONE);
            llAge2.setVisibility(view.VISIBLE);
            llAge3.setVisibility(view.GONE);
            llAge4.setVisibility(view.GONE);
            llAge5.setVisibility(view.GONE);


            final Calendar c = Calendar.getInstance();
            editTextCurrentYear2.setText(String.valueOf(c.get(Calendar.YEAR)));
            editTextCurrentMonth2.setText(addZero(c.get(Calendar.MONTH) + 1));
            editTextCurrentDay2.setText(addZero(c.get(Calendar.DAY_OF_MONTH)));

            SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
            Date date = new Date(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH) - 1);



        }


        if(position==2){
            knopki1.setVisibility(view.GONE);
            knopki2.setVisibility(view.GONE);
            knopki3.setVisibility(view.VISIBLE);
            knopki4.setVisibility(view.GONE);
            knopki5.setVisibility(view.GONE);
            LinearLayout022.setVisibility(view.VISIBLE);
            LinearLayout023.setVisibility(view.VISIBLE);
            LinearLayout024.setVisibility(view.GONE);
            LinearLayout025.setVisibility(view.GONE);
            llAge.setVisibility(view.GONE);
            llAge2.setVisibility(view.GONE);
            llAge3.setVisibility(view.VISIBLE);
            llAge4.setVisibility(view.GONE);
            llAge5.setVisibility(view.GONE);

            final Calendar c = Calendar.getInstance();
            editTextCurrentYear3.setText(String.valueOf(c.get(Calendar.YEAR)));
            editTextCurrentMonth3.setText(addZero(c.get(Calendar.MONTH) + 1));
            editTextCurrentDay3.setText(addZero(c.get(Calendar.DAY_OF_MONTH)));

            SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
            Date date = new Date(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH) - 1);



        }

        if(position==3){
            knopki1.setVisibility(view.GONE);
            knopki2.setVisibility(view.GONE);
            knopki3.setVisibility(view.GONE);
            knopki4.setVisibility(view.VISIBLE);
            knopki5.setVisibility(view.GONE);
            LinearLayout022.setVisibility(view.VISIBLE);
            LinearLayout023.setVisibility(view.VISIBLE);
            LinearLayout024.setVisibility(view.VISIBLE);
            LinearLayout025.setVisibility(view.GONE);
            llAge.setVisibility(view.GONE);
            llAge2.setVisibility(view.GONE);
            llAge3.setVisibility(view.GONE);
            llAge4.setVisibility(view.VISIBLE);
            llAge5.setVisibility(view.GONE);





            final Calendar c = Calendar.getInstance();
            editTextCurrentYear4.setText(String.valueOf(c.get(Calendar.YEAR)));
            editTextCurrentMonth4.setText(addZero(c.get(Calendar.MONTH) + 1));
            editTextCurrentDay4.setText(addZero(c.get(Calendar.DAY_OF_MONTH)));

            SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
            Date date = new Date(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH) - 1);



        }


        if(position==4){
            knopki1.setVisibility(view.GONE);
            knopki2.setVisibility(view.GONE);
            knopki3.setVisibility(view.GONE);
            knopki4.setVisibility(view.GONE);
            knopki5.setVisibility(view.VISIBLE);
            LinearLayout022.setVisibility(view.VISIBLE);
            LinearLayout023.setVisibility(view.VISIBLE);
            LinearLayout024.setVisibility(view.VISIBLE);
            LinearLayout025.setVisibility(view.VISIBLE);
            llAge.setVisibility(view.GONE);
            llAge2.setVisibility(view.GONE);
            llAge3.setVisibility(view.GONE);
            llAge4.setVisibility(view.GONE);
            llAge5.setVisibility(view.VISIBLE);





            final Calendar c = Calendar.getInstance();
            editTextCurrentYear5.setText(String.valueOf(c.get(Calendar.YEAR)));
            editTextCurrentMonth5.setText(addZero(c.get(Calendar.MONTH) + 1));
            editTextCurrentDay5.setText(addZero(c.get(Calendar.DAY_OF_MONTH)));

            SimpleDateFormat simpledateformat = new SimpleDateFormat("EEEE");
            Date date = new Date(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH) - 1);



        }


      //  Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {



    }
}